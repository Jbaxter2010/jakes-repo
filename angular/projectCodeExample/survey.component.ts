import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.css']
})
export class SurveyComponent implements OnInit {

  options:string[]= [
    "Best time",
    "Next Best time",
    "Not as good",
    "Worst time"
  ];

  classTimes:string[] = [
    "Monday/Wednesday 10:10am-Noon",
    "Tuesday 6:00pm-9:00pm",
    "Wednesday 6:00pm-9:00pm",
    "Tuesday/Thursday 10:10am-Noon"
  ];

  email:string = '';

  constructor() { }

  ngOnInit() {
  }

  onChange($event, options:string[]){
  let removeValue:number = $event.target.value;
  let className:string = $event.target.className;
  document.getElementById(className).innerHTML = options[removeValue];
  options.splice(removeValue,1); 
  }

  onReset(){
    this.options = [
      "Best time",
      "Next Best time",
      "Not as good",
      "Worst time"
    ];
    this.email = '';
  }
}
