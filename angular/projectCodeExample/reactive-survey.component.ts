import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'; // Make sure to import FormBuilder, FormGroup and Validators for this code.

import { ratings, Times, surveyInfo } from '../data-model';          // Import the const 'ratings' array of values, the 'Times' data structure class, 
                                                                     //       and the SurveyInfo data structure class form data-model 

@Component({
  selector: 'app-reactive-survey',
  templateUrl: './reactive-survey.component.html',
  styleUrls: ['./reactive-survey.component.css']
})
export class ReactiveSurveyComponent implements OnInit {

  surveyForm: FormGroup;
  ratings = ratings;                              // Set ratings property equal to the value of imported ratings array.
  value ='';                                      // property to store event.target values. // Used to find indexOf(value), and splice(value)
  option1Rating='';                               // Used to interpolate form-control 'option1' selected value into the HTML
  option2Rating='';                               // Used to interpolate form-control 'option2' selected value into the HTML
  option3Rating='';                               // Used to interpolate form-control 'option3' selected value into the HTML
  option4Rating='';                               // Used to interpolate form-control 'option4' selected value into the HTML

  constructor(private fb: FormBuilder) {          // Set private FormBuilder in constructor parameters
    this.createForm();                            // Creating form through the constructor
   }

  createForm() {                                  // Function to create our form
     this.surveyForm = this.fb.group({            // Declares surveyForm as the top-level formGroup
      email: ['', Validators.required],           // Declares email as a form-control
      times: this.fb.group(new Times())           // Declares times as a nested formGroup within surveyForm. 
                                                  // Sets formGroup 'times' as type 'Times' (imported class from data-model.ts) 
     });
  }


  onChange(event: any) {                          // Function bound to 'on change' event tied to the target select element. Parameters include the $event payload type: any
    var targetId = event.target.id;               // Grab and store the target element id. Will need for if/else statement below.
    //alert(targetId); Testing
    
    this.value = event.target.value;              // Grab and store target value.
    //alert(this.value); Testing

    var found = this.ratings.indexOf(this.value); // Storing the index of the target value in a variable
    while (found !== -1){                         // As long as the target.value's index is not equal to -1, execute the following
       this.ratings.splice(found,1);              // Remove this target.value from the ratings(array) property 
       found = this.ratings.indexOf(this.value);  // While target.value index is not equal to -1, found = this target.value's index.
      }; 

    if (targetId == 'option1select'){             // If/Else statement to set correct select element value with corresponding property for interpolation.
      this.option1Rating = this.value;            // Example, if targetId == 'option1select' , then set this target.value as property option1select value.
    }                                             // This will then use interpolation to show the value on the HTML page where {{option1select}} is located.
    else if (targetId == 'option2select'){
      this.option2Rating = this.value;
    }
    else if (targetId == 'option3select'){
      this.option3Rating = this.value;
    } 
    else if (targetId == 'option4select'){
      this.option4Rating = this.value;
    } 
  }

  onSubmit() {                                    // Function to submit the form. For this project, no submission is made. Alerts function as a test for form validation.
    
    //const formModel = this.surveyForm.value;
    if (this.surveyForm.valid){                   // If this form is valid, execute the following.
      alert("Form Submitted");
      this.onReset();                             // Call onReset() function to reset the form after submission.
    }
    else {                                        // If this form is invalid, alert the user.
      alert("Invalid Form");
    }
  }
                                                  /*  Something is goofy with this onReset function. I don't think the .reset() function data-structure is correct, but I will explain how I believe it SHOULD work */
  onReset(){                                      // Function to reset the form.
    this.surveyForm.reset(                        // select the top-level formGroup as declared in your createForm() function, and execute the .reset() function.
      {                                           // .reset() requires you to enter new information for every form-Control in your form. The data entered must match the data structure 
        email: '',                                //        used when creating the form. 
        times:{                                   // This data-set identifies the form-control 'email' and sets the value, the 'times' formGroup, and duplicates the structure of 
          option1: ratings,                       //        the 'times' formGroup. The form-controls within 'times' are identified and given values. In this case the values are 
          option2: ratings,                       //        entered as the imported class 'ratings', an array with four values.
          option3: ratings,                       // This process resets the form with the values entered, and in this case, creates a 'new' or 'default' form. 
          option4: ratings}
      }
    );
                                                  /* I don't think you need this next block of code if your code block above is correctly structured. However, my function will not work unless I have both. */
    this.ratings = ['Best time',                  // This code sets the property 'ratings' back to being equal to the original ratings array. 
    'Next Best time',                             // Must enter this or you will not have options for you select elements.
    'Not so good', 
    'Worst time']

    this.option1Rating='';                        // Clears the properties used for interpolation. This ensures the values are cleared in the HTML. 
    this.option2Rating='';
    this.option3Rating='';
    this.option4Rating='';    
  }


  ngOnInit() {
  }

}
