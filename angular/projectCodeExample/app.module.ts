import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { SurveyComponent } from './survey/survey.component';
import { ReactiveSurveyComponent } from './reactive-survey/reactive-survey.component';


@NgModule({
  declarations: [
    AppComponent,
    SurveyComponent,
    ReactiveSurveyComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
