<!doctype html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Class Example 1</title>

</head>

<body>
    <!-- PHP OOP
    Class Emailer
    -->

    <!-- CLASS (blueprint)
         Instructions/How to. Tells you what an object will do.

        * Instantiation * (Turning a class into an object)
            $email = new Emailer();
        
    -->

    <?php 
        //Emailer Class definition
    
        class Emailer {
            
            //Set properties to private or public. When in doubt, set to private. Must set one or the other.
            
            private $sendTo="";
            private $sentFrom="";
            private $emailSubject="";
            private $emailMsg="";
           
            
            //Constructors in php start with double underscore. 
            
            public function __construct()
            {
                
            }
            
            /* Setters and getters:
                    Setters = input a value / set a property
                    Getters = 
            */
            
            public function setSendTo($inSendTo)
            {
                $this->sendTo = $inSendTo;
                
            }
            
            public function setSentFrom($inSentFrom)
            {
                $this->sentFrom = $inSentFrom;
            }
            
            public function setEmailSubject($inEmailSubject)
            {
                $this->emailSubject = $inEmailSubject;
            }
            
            public function setEmailMsg($inEmailMsg)
            {
                $inEmailMsg = htmlentities($inEmailMsg); //handles special characters
                $inEmailMsg = wordwrap($inEmailMsg, 70, "\n"); //sentence length 70 characters and breaks
                $this->emailMsg = $inEmailMsg; 
            }
            
            public function getSendTo()
            {
                return $this->sendTo;
            }
            
            public function getSentFrom()
            {
                return $this->sentFrom;
            }
            
            public function getEmailSubject()
            {
                return $this->emailSubject;
            }
            
            public function getEmailMsg()
            {
                return $this->emailMsg;
            }
            
            public function sendEmail()
            {
                $headers = "From: $this->sentFrom" . "\r\n";
                //echo "<h2>Headers $headers</h2>";
                //echo "<h2>Message $this->emailMsg</h2>";
                return mail($this->sendTo,$this->emailSubject,$this->emailMsg,$headers);
            }
            
        }
        
    ?>



</body>

</html>
