<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $database = "wdv341";
    
    try {
       $con = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
       // set the PDO error mode to exception
       $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       //echo "Connected successfully";
    }
    
    catch(PDOException $e)
       {
       echo "Connection failed: " . $e->getMessage();
       }
  /* $con = new PDO('mysql:host=localhost;dbname=mysql:charset=utf8', 'root', '');
   $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   $con->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); 
    */
?>