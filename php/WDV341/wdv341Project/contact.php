<?php
     session_start();
    //Variables
    
        //field data
        $contact_name = "";
        $contact_email = "";
        $contact_comment= "";
        $contact_test = "";

        //error messages
        $nameErrMsg = "";
        $emailErrMsg = "";
        $commentErrMsg = "";
        

        $validForm = false;

    if(isset($_POST["submit"]))
    {
        //The form has been submitted and needs to be processed
        
        //Validate the form data here
        
        //Get the name value pairs from the $_POST variable into PHP variables
		//This example uses PHP variables with the same name as the name atribute from the HTML form
        
        $contact_name = $_POST['contactName'];
        $contact_email = $_POST['contactEmail'];
        $contact_comment = $_POST['contactComment'];
        $contact_test = $_POST['contactTest'];
        
        /*	FORM VALIDATION PLAN
		
			FIELD NAME		VALIDATION TESTS & VALID RESPONSES
			Name		    Required Field		May not be empty, any leading spaces should be removed.
			Security		Required Field		May not be empty, must be numeric, no hyphens or (), must be right size.
            Response        Required Field      One response must be selected. 
            Honey pot       Not Required        If filled, do not submit data. 
            
		*/
        
        //VALIDATION FUNCTIONS      Use functions to contain the code for the field validations.
            function validateName($inName)
            {
                global $validForm, $nameErrMsg, $contact_name; //Use the GLOBAL version of these variables instead of making them local
                
                $nameErrMsg = "";
                
                if ( strlen(trim($inName)) <= 0 )
                {
                    $validForm = false;
                    $nameErrMsg = "Required field. Name cannot be spaces";
                }
                else
                {
                    $contact_name = trim($contact_name);
                }
            }// end validateName()
        
            function validateEmail($inEmail)
            {
                global $validForm, $emailErrMsg; //Use the GLOBAL version of these variables instead of making them local
                
                $emailErrMsg = "";
                
                if (filter_var($inEmail, FILTER_VALIDATE_EMAIL)){
                    //valid email
                }
                else{
                    $validForm = false;
                    $emailErrMsg = "Required field. Please enter a valid Email Address";
                }
               
            }// end validateEmail()
        
            function validateComment($inComment)
            {
                global $validForm, $commentErrMsg; //Use the GLOBAL version of these variables instead of making them local
                
                $commentErrMsg = "";
                
                if($inComment == "")
                {
                    $validForm = false;
                    $commentErrMsg = "Required field. Please leave a comment";
                }
            }// End validateComment()
        
            function validateTest($inTest)
            {
                global $validForm;
                
                if (!$inTest == "")
                {
                    $validForm = false;
                }
            } //End validateTest()
        
            //VALIDATE FORM DATA using functions defined above
        $validForm = true;        //Switch for keeping track of any form validation errors
        
        validateName($contact_name);
        validateEmail($contact_email);
        validateComment($contact_comment);
        validateTest($contact_test);
        
        if($validForm)
        {
            Echo "testing works?";
            include 'emailClass.php';


            $formDataMsg = "User's name: ".$_POST["contactName"]."\r\n"."User's Email: ".$_POST["contactEmail"]."\r\n"."User's Comment: ".$_POST["contactComment"];
            //echo $formDataMsg."<br />";
         
        
            $formDataEmail = new Emailer();
        
                $formDataEmail->setSendTo("jrbaxter@dmacc.edu"); //Set sendTo property
        
                //echo "Send to: ".$formDataEmail->getSendTo()."<br>"; //echo value of sendTo property using getSendTo function
        
        
        
                $formDataEmail->setSentFrom("contact@jakebaxter.info"); //Set sentFrom property
        
                //echo "Sent from: ".$formDataEmail->getSentFrom()."<br>"; //echo value of sentFrom property using getSentFrom function
        
        
        
                $formDataEmail->setEmailSubject("PHP Project Contact Form Submission");
        
                //echo "Subject: ".$formDataEmail->getEmailSubject()."<br>";
        
        
        
                $formDataEmail->setEmailMsg($formDataMsg);
        
        
                //echo "Message: ".$formDataEmail->getEmailMsg()."<br>";
        
        
                $formDataEmail->sendEmail(); //Create and send an email. *Remember Heartland Web Hosting requires a domain email in the sentFrom property.
        
        
        
        
            // Confirmation email to be sent to form submitter. 
            $userConfirmation = new Emailer();
        
                $userConfirmation->setSendTo($_POST["contactEmail"]); //Set sendTo property
        
                //echo "Send to: ".$userConfirmation->getSendTo()."<br>"; //echo value of sendTo property using getSendTo function
        
        
        
                $userConfirmation->setSentFrom("contact@jakebaxter.info"); //Set sentFrom property
        
                //echo "Sent from: ".$userConfirmation->getSentFrom()."<br>"; //echo value of sentFrom property using getSentFrom function
        
        
        
                $userConfirmation->setEmailSubject("Form Submission Confirmation");
        
                //echo "Subject: ".$userConfirmation->getEmailSubject()."<br>";
        
        
        
                $userConfirmation->setEmailMsg("Thank you for taking the time to complete the contact form. \r\n"."We have successfully received your submission! \r\n"."A member of our team will be in touch as soon as possible.");
        
        
        
               //echo "Message: ".$userConfirmation->getEmailMsg()."<br>";
        
        
                $userConfirmation->sendEmail(); //Create and send an email. *Remember Heartland Web Hosting requires a domain email in the sentFrom property.
            $successMessage = "Success!";
          
        }
        else
        {
            $message = "Something went wrong. Please try again";
        }
        
    }//Ends if submit

    else
    {
        //Form has not been seen by the user. Display the form
    }
   
    ?>
    <!doctype html>
    <html>
    <head>
        
        
        <title>Contact</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <style>
            .formatFieldset{
                width: 35%;
            }
            .required:before{
                content:"*";
                font-weight:bold;
                color:red;
                
            }
                /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
                .row.content {height: 1500px}
            
            /* Set gray background color and 100% height */
            .sidenav {
            background-color: #f1f1f1;
            height: 100%;
            }
            
            /* Set black background color, white text and some padding */
            footer {
            background-color: #555;
            color: white;
            padding: 15px;
            }
            
            /* On small screens, set height to 'auto' for sidenav and grid */
            @media screen and (max-width: 767px) {
            .sidenav {
                height: auto;
                padding: 15px;
            }
            .row.content {height: auto;} 
            }

            #contactTest {
                    display: none;
                }
                
            .error	{
                color:red;
                font-style:italic;	
            }
            .success {
                color:green;
                font-style:italic;
            }
        </style>

        

        
    </head>
        
    <body>
    <div class="container-fluid">
                <div class="row content">
                <div class="col-sm-3 sidenav">
                <h4>My Club's Members List</h4>
                <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="index.php">Home</a></li>
                    <li><a href="contact.php">Contact</a></li>
                    <?php
                        if ($_SESSION['valid'] == true) {
                    ?>
                            <li><a href="add.php">Add New Member</a></li>
                    <?php
                            }else{
                            }
                    ?>
                    
                    <li><a href="login.php"><span class="glyphicon glyphicon-log-in"></span>Admin Login</a></li>
                    <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span>Log Out</a></li>
                </ul><br>
                </div>

                <div class="col-sm-9">
                        <h2>Please enter your contact information and a short comment.</h2>
                        
                        <form name="form1" id="form1" method="post" action="contact.php">
                            <fieldset class="formatFieldset">
                                <legend> Contact Form </legend>
                                <label for="contactName" class="required"><span></span>Name:</label>
                                <input type="text" name="contactName" placeholder="your name here" value="<?php $contact_name?>" id="contactName" required><span class="error"><?php echo $nameErrMsg ?></span>
                                <br>
                                <br>
                                <label for="contactEmail" class="required">Email:</label>
                                <input type="text" name="contactEmail" placeholder="email@outlook.com" value="<?php $contact_email ?>" required><span class="error"><?php echo $emailErrMsg ?></span>
                                <br>
                                <br>
                                <label for="contactComment" class="required">Comments:</label>
                                <br>
                                <textarea cols="50" rows="5" maxlength="500" name="contactComment" id="contactComment" placeholder="enter your comments here" required><?php $contact_comment ?></textarea><span class="error"><?php echo $commentErrMsg ?></span>
                            </fieldset>

                            <!-- Honey Pot field -->
                            <input type="text" name="contactTest" id="contactTest" value="";>
                        
                            <input type="submit" name="submit" value="Submit">
                            <input type="reset" value="Reset">
                        </form>
                        <span class="error"><?php echo $message ?></span>
                        <span class="success"><?php echo $successMessage ?></span>
                    </div>
        </div>
    </div>
    <footer class="container-fluid">
        <p>Footer Text</p>
    </footer>
    </body>    
        
    </html>