<?php
     session_start();
     //Only allow a valid user access to this page
     if ($_SESSION['valid'] !== true) {
         header('Location: login.php');
     }
     else{
        require_once('dbcon.php');
        if(isset($_GET['id'])){
            $id = $_GET['id'];
            try{
                $stmt = $con->prepare("DELETE FROM tb_members WHERE member_id=$id");
                $stmt->execute(array($id));
                header('Location:index.php');
            }
            catch(PDOException $ex){
                echo $ex->getMessage();
            }
        }
    }

?>