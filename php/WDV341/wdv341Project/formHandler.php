<?php

    include 'emailClass.php';


    $formDataMsg = "User's name: ".$_POST["contactName"]."\r\n"."User's Email: ".$_POST["contactEmail"]."\r\n"."User's Comment: ".$_POST["contactComments"];
    //echo $formDataMsg."<br />";
 

    $formDataEmail = new Emailer();

        $formDataEmail->setSendTo("jrbaxter@dmacc.edu"); //Set sendTo property

        //echo "Send to: ".$formDataEmail->getSendTo()."<br>"; //echo value of sendTo property using getSendTo function



        $formDataEmail->setSentFrom("contact@jakebaxter.info"); //Set sentFrom property

        //echo "Sent from: ".$formDataEmail->getSentFrom()."<br>"; //echo value of sentFrom property using getSentFrom function



        $formDataEmail->setEmailSubject("WDV341 Contact Form Submission");

        //echo "Subject: ".$formDataEmail->getEmailSubject()."<br>";



        $formDataEmail->setEmailMsg($formDataMsg);


        //echo "Message: ".$formDataEmail->getEmailMsg()."<br>";


        $formDataEmail->sendEmail(); //Create and send an email. *Remember Heartland Web Hosting requires a domain email in the sentFrom property.




    // Confirmation email to be sent to form submitter. 
    $userConfirmation = new Emailer();

        $userConfirmation->setSendTo($_POST["contactEmail"]); //Set sendTo property

        //echo "Send to: ".$userConfirmation->getSendTo()."<br>"; //echo value of sendTo property using getSendTo function



        $userConfirmation->setSentFrom("contact@jakebaxter.info"); //Set sentFrom property

        //echo "Sent from: ".$userConfirmation->getSentFrom()."<br>"; //echo value of sentFrom property using getSentFrom function



        $userConfirmation->setEmailSubject("Form Submission Confirmation");

        //echo "Subject: ".$userConfirmation->getEmailSubject()."<br>";



        $userConfirmation->setEmailMsg("Thank you for taking the time to complete the contact form. \r\n"."We have successfully received your submission! \r\n"."A member of our team will be in touch as soon as possible.");



       //echo "Message: ".$userConfirmation->getEmailMsg()."<br>";


        $userConfirmation->sendEmail(); //Create and send an email. *Remember Heartland Web Hosting requires a domain email in the sentFrom property.

    if( $_POST["contactName"] || $_POST["contactEmail"] || $_POST["contactComments"])
    {
    

    ?>
    <!DOCTYPE html>
    <html>
    <head>
    <title>PHP Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
        .row.content {height: 1500px}
        
        /* Set gray background color and 100% height */
        .sidenav {
        background-color: #f1f1f1;
        height: 100%;
        }
        
        /* Set black background color, white text and some padding */
        footer {
        background-color: #555;
        color: white;
        padding: 15px;
        }
        
        /* On small screens, set height to 'auto' for sidenav and grid */
        @media screen and (max-width: 767px) {
        .sidenav {
            height: auto;
            padding: 15px;
        }
        .row.content {height: auto;} 
        }
    </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row content">
                <div class="col-sm-3 sidenav">
                <h4>My Club's Members List</h4>
                <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="index.php">Home</a></li>
                    <li><a href="contact.php">Contact</a></li>
                    <?php
                        if ($_SESSION['valid'] == true) {
                    ?>
                            <li><a href="add.php">Add New Member</a></li>
                    <?php
                            }else{
                            }
                    ?>
                    
                    <li><a href="login.php"><span class="glyphicon glyphicon-log-in"></span>Admin Login</a></li>
                    <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span>Log Out</a></li>
                </ul><br>
                </div>
            
        
                <div class="col-sm-9">

                <?php
                echo "<h2>Welcome ". $_POST['contactName']. "!</h2>";
                echo "<h3>Your email: ". $_POST["contactEmail"]. "</h3>";
                echo "<h3>Your comment: ". $_POST["contactComments"]."</h3>";
                echo "<h3> Your email has been sent! </h3>";
                ?>
                </div>
            </div>
        </div>
        <footer class="container-fluid">
        <p>Footer Text</p>
        </footer>

    </body>
    </html>
    <?php
    }
    ?>
    