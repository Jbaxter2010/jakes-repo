<?php
    
    include 'emailClass.php';


    
    $newEmail = new Emailer(); //instantiate a new object/variable



    $newEmail->setSendTo("jrbaxter@dmacc.edu"); //Set sendTo property

    echo "Send to: ".$newEmail->getSendTo()."<br>"; //echo value of sendTo property using getSendTo function



    $newEmail->setSentFrom("contact@jakebaxter.info"); //Set sentFrom property

    echo "Sent from: ".$newEmail->getSentFrom()."<br>"; //echo value of sentFrom property using getSentFrom function



    $newEmail->setEmailSubject("TEST");

    echo "Subject: ".$newEmail->getEmailSubject()."<br>";



    $newEmail->setEmailMsg("Hello World. This is a sample email message.");

    echo "Message: ".$newEmail->getEmailMsg()."<br>";


    $newEmail->sendEmail(); //Create and send an email. *Remember Heartland Web Hosting requires a domain email in the sentFrom property.

    
    echo "<h2> Your email has been sent! </h2>";

?>
