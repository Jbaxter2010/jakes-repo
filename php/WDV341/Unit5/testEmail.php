<?php
    
    include 'emailClass.php';


    
    $newEmail = new Emailer(); //instantiate a new object/variable



    $newEmail->setSendTo("jrbaxter@dmacc.edu"); //Set sendTo property

    echo $newEmail->getSendTo()."<br>"; //echo value of sendTo property using getSendTo function



    $newEmail->setSentFrom("contact@jakebaxter.info"); //Set sentFrom property

    echo $newEmail->getSentFrom()."<br>"; //echo value of sentFrom property using getSentFrom function



    $newEmail->setEmailSubject("TEST");

    echo $newEmail->getEmailSubject()."<br>";



    $newEmail->setEmailMsg("Hello World. This is a sample email message.");

    echo $newEmail->getEmailMsg()."<br>";


    $newEmail->sendEmail(); //Create and send an email. *Remember Heartland Web Hosting requires a domain email in the sentFrom property.


?>
