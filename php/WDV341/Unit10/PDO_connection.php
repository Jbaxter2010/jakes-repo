<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "wdv341";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully"; 
    }

catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }

try {
    // prepare sql and bind parameters
    $stmt = $conn->prepare("INSERT INTO wdv341_event (event_name, event_description, event_presenter) 
    VALUES (:eventname, :eventdescription, :eventpresenter)");
    $stmt->bindParam(':eventname', $eventName);
    $stmt->bindParam(':eventdescription', $eventDescription);
    $stmt->bindParam(':eventpresenter', $eventPresenter);
    
    // insert a row
    $eventName = "Work";
    $eventDescription = "Early Morning Stocker";
    $eventPresenter = "Jake Baxter";
    $stmt->execute();
    
    echo "New records created successfully";
    }

catch(PDOException $e)
    {
    echo "Error: " . $e->getMessage();
    }

$conn = null;
?>