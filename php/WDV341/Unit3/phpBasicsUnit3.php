<!doctype html>
<html>    
<head>
        
    <!-- WDV341 PHP
         Jake Baxter
         1/29/2018
    -->
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PHP Basics</title> 
    
    <?php 
        
        $yourName = "Jake Baxter";
        $number1 = "50";
        $number2 = "100";
        $total = $number1 + $number2;
        $myArray = array("PHP", "HTML", "Javascript");
    ?>
    
    <?php
        function displayArray()
        {
            global $myArray;
            $myArrayLength = count($myArray);
            for ($i=0; $i<$myArrayLength; $i++)
            {
                echo $myArray[$i];
                echo "<br>";
            } ;
        }
    ?>
    
</head>
    
<body>
    <?php echo "<h1>PHP Basics</h1>"; ?>
    
    <h2><?php echo $yourName; ?></h2>
	
	<?php echo $number1, "<br>"; ?>
	<?php echo $number2, "<br>"; ?>
	<?php echo $number1, " "."+"." ".$number2." "."="." ".$total."<br>"; ?>
    
    <!-- Confused on how the assignment wants us to create and display the array (Use PHP array function? Javascript array function?); going to provide a couple options. 
    -->
    
    <!-- Option 1. -->
    <?php echo '<p>'.'myArray values are: '.
    $myArray[0].' '.$myArray[1].' '.$myArray[2].'</p>'; ?>
    
    
    <!-- Option 2. -->
    <?php
        $myArrayLength = count($myArray);
        for ($i=0; $i<$myArrayLength; $i++)
            {
                echo $myArray[$i];
                echo "<br>";
            } ;
    ?>
    
    
    <!-- Option 3. -->
    <p><?php echo displayArray(); ?></p>
    
    
    <!-- Option 4. -->
    <script>
        <?php echo "var jsArray = ['PHP', 'HTML', 'Javascript'];"; ?>
        <?php echo "for (i=0; i<jsArray.length; i++)
			{
				var arrayElement = jsArray[i]
                document.write(arrayElement+'<br>');
			}"; ?>
    </script>
    
  
    
    
</body>
    
</html>