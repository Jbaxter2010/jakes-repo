<?php 
    
    session_start();
    
    require_once('dbcon.php');
    ?>
    <!DOCTYPE html>
    <html>
    <head>
    <title>PHP Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
        .row.content {height: 1500px}
        
        /* Set gray background color and 100% height */
        .sidenav {
        background-color: #f1f1f1;
        height: 100%;
        }
        
        /* Set black background color, white text and some padding */
        footer {
        background-color: #555;
        color: white;
        padding: 15px;
        }
        
        /* On small screens, set height to 'auto' for sidenav and grid */
        @media screen and (max-width: 767px) {
        .sidenav {
            height: auto;
            padding: 15px;
        }
        .row.content {height: auto;} 
        }
    </style>
    </head>
    <body>
         <div class="container-fluid">
            <div class="row content">
               <div class="col-sm-3 sidenav">
               <h4>My Club's Member List</h4>
               <ul class="nav nav-pills nav-stacked">
                  <li class="active"><a href="index.php">Home</a></li>
                  <li><a href="contact.php">Contact</a></li>
                  <?php
                     if ($_SESSION['valid'] == true) {
                  ?>
                        <li><a href="add.php">Add New Member</a></li>
                  <?php
                        }else{
                        }
                  ?>
                  
                  <li><a href="login.php"><span class="glyphicon glyphicon-log-in"></span>Admin Login</a></li>
                  <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span>Log Out</a></li>
               </ul><br>
               </div>
            
         
            <div class="col-sm-9">
                <?php
                    $stmt = $con->prepare("SELECT * FROM tb_members ORDER BY member_id ASC");
                    $stmt->execute();
                    $results = $stmt->fetchAll();
                    foreach($results as $row){
                ?>
                     
                        <hr>
                        <h2><?=$row['member_name'];?></h2>
                        <h5><span class="label label-success">Age: <?=$row['member_age'];?></span></h5>
                        <h4><small>Member id: <?=$row['member_id'];?></small></h4>
                        <h5><span class="glyphicon glyphicon-time"></span> Added by Admin, <?=$row['member_date'];?> </h5>
                       
                        <h5>About Me:</h5>
                        <p><?=$row['member_bio'];?></p>
                        <?php
                        if ($_SESSION['valid'] == true) {
                           ?>
                              <button class=".btn"><a  href="edit.php?id=<?=$row['member_id'];?>">Edit</a></button>
                              <button class=".btn"><a  href="delete.php?id=<?=$row['member_id'];?>">Delete</a></button>
                           <?php
                           }else{
                           }
                           ?>
                        <hr>
                        
                <?php
                    }
                ?>
            </div>
            </div>
         </div>
        <footer class="container-fluid">
        <p>My Club</p>
        </footer>

</body>
</html>



    <?php
    
    ?>


