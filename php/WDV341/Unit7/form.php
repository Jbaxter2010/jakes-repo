<?php

 // WDV341 Self Posting Form - Jake Baxter - 3/5/2018
    
    //Variables
    
        //field data
        $form1_name = "";
        $form1_security = "";
        $form1_response = "";

        //error messages
        $nameErrMsg = "";
        $securityErrMsg = "";
        $responseErrMsg = "";
        

        $validForm = false;

    if(isset($_POST["submit"]))
    {
        //The form has been submitted and needs to be processed
        
        //Validate the form data here
        
        //Get the name value pairs from the $_POST variable into PHP variables
		//This example uses PHP variables with the same name as the name atribute from the HTML form
        
        $form1_name = $_POST['form1_name'];
        $form1_security = $_POST['form1_security'];
        $form1_response = $_POST['form1_response'];
        
        /*	FORM VALIDATION PLAN
		
			FIELD NAME		VALIDATION TESTS & VALID RESPONSES
			Name		    Required Field		May not be empty
			Security		Required Field		May not be empty
            Response        Required Field      May not be empy
            
		*/
        
        //VALIDATION FUNCTIONS      Use functions to contain the code for the field validations.
            function validateName($inName)
            {
                global $validForm, $nameErrMsg; //Use the GLOBAL version of these variables instead of making them local
                
                if($inName == "")
                {
                    $validForm = false;
                    $nameErrMsg = "Required field. Name cannot be spaces";
                }
            }// end validateName()
        
            function validateSecurity($inSecurity)
            {
                global $validForm, $securityErrMsg; //Use the GLOBAL version of these variables instead of making them local
                
                if($inSecurity == "")
                {
                    $validForm = false;
                    $securityErrMsg = "Required field. Please enter your Social Security number";
                }
            }// end validateSecurity()
        
            function validateResponse($inResponse)
            {
                global $validForm, $responseErrMsg; //Use the GLOBAL version of these variables instead of making them local
                
                if($inResponse == "")
                {
                    $validForm = false;
                    $responseErrMsg = "Required field. Please select a response";
                }
            }// End validateResponse()
        
            //VALIDATE FORM DATA using functions defined above
        $validForm = true;        //Switch for keeping track of any form validation errors
        
        validateName($form1_name);
        validateSecurity($form1_security);
        validateResponse($form1_response);
        
        if($validForm)
        {
            $message = "Success!"."<br>"."<a href = 'form.php'>New Form</a>";
        }
        else
        {
            $message = "Something went wrong. Please try again"."<br>"."<a href = 'form.php'>New Form</a>";
        }
        
    }//Ends if submit
    
    else
    {
        //Form has not been seen by the user. Display the form
    }
   

?>
    
<!DOCTYPE html>
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WDV341 Intro PHP - Self Posting Form</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
    
        
    });
</script>
    
    
<style>

#orderArea	{
	width:600px;
	background-color:#CF9;
}

.error	{
	color:red;
	font-style:italic;	
}
</style>
</head>

<body>
<h1>WDV341 Intro PHP</h1>
<h2><a href = "form.php">Self Posting Form</a>
    <?php
            //If the form was submitted and valid and properly put into database display the INSERT result message
			if($validForm)
			{
        ?>
            <h1><?php echo $message ?></h1>
        
        <?php
			}
			else	//display form
            {
    ?>

</h2>
<div id="orderArea">
  <form id="form1" name="form1" method="post" action="form.php">
  <h3>Customer Registration Form</h3>
  <table width="587" border="0">
    <tr>
      <td width="117">Name:</td>
      <td width="246"><input type="text" name="form1_name" id="form1_name" size="40" value="<?php echo $form1_name; ?>"/></td>
      <td width="210" class="error"><?php echo $nameErrMsg; ?></td>
    </tr>
    <tr>
      <td>Social Security</td>
      <td><input type="text" name="form1_security" id="form1_security" size="40" value="<?php echo $form1_security; ?>" /></td>
      <td class="error"><?php echo $securityErrMsg; ?></td>
    </tr>
    <tr>
      <td>Choose a Response</td>
      <td><p>
        <label>
          <input type="radio" name="form1_response" id="form1_response0">
          Phone</label>
        <br>
        <label>
          <input type="radio" name="form1_response" id="form1_response1" checked="checked">
          Email</label>
        <br>
        <label>
          <input type="radio" name="form1_response" id="form1_response2">
          US Mail</label>
        <br>
      </p></td>
      <td class="error"><?php echo $responseErrMsg; ?></td>
    </tr>
  </table>
  <p>
    <input type="submit" name="submit" id="button" value="Register" />
    <input type="reset" name="button2" id="reset_form" value="Clear Form" />
  </p>
</form>
</div>
    <?php
        } //end else
    ?>

</body>
</html>