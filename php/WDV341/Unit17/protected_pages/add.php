<?php
    session_start();
    //Only allow a valid user access to this page
    if ($_SESSION['valid'] !== true) {
	    header('Location: login.php');
    }
    else{
        require_once('dbcon.php');

        $name = "";
        $age = "";
        $bio = "";
        $date = '';

        //err messages
        $nameErrMsg = "";
        $ageErrMsg = "";
        $bioErrMsg = "";
        $dateErrMsg = "";

        $validForm = false;

        if(isset($_POST['btn_submit'])){
            //echo 'submitted true';// test
            $name = $_POST['txt_name'];
            $age = $_POST['txt_age'];
            $bio = $_POST['txt_bio'];
            $date = $_POST['txt_date'];


            //VALIDATION FUNCTIONS      Use functions to contain the code for the field validations.
            function validateName($inName)
            {
                global $validForm, $nameErrMsg, $name; //Use the GLOBAL version of these variables instead of making them local
                
                $nameErrMsg = "";
                
                if ( strlen(trim($inName)) <= 0 )
                {
                    $validForm = false;
                    $nameErrMsg = "Required field. Name cannot be spaces";
                }
                else
                {
                    $name = trim($name);
                }
            }// end validateName()

            
            function validateAge($inAge)
            {
                global $validForm, $ageErrMsg, $age; //Use the GLOBAL version of these variables instead of making them local
                
                $ageErrMsg = "";
                
                if (filter_var($inAge, FILTER_VALIDATE_INT))
                {
                   //Valid Age
                }
                else
                {
                    $validForm = false;
                    $ageErrMsg = "Required field. Age must be an integer";
                }
            }// end validateAge()

            function validateBio($inBio)
            {
                global $validForm, $bioErrMsg; //Use the GLOBAL version of these variables instead of making them local
                
                $bioErrMsg = "";
                
                if($inBio == "")
                {
                    $validForm = false;
                    $bioErrMsg = "Required field. Please enter a Bio";
                }
            }// End validateBio()

            function validateDate($inDate)
            {
                global $validForm, $dateErrMsg; //Use the GLOBAL version of these variables instead of making them local
                
                $dateErrMsg = "";

                if($inDate == "")
                {
                    $validForm = false;
                    $dateErrMsg = "Required field. Please enter a Date";
                }
            }// End validateDate()

            $validForm = true;

            validateName($name);
            validateAge($age);
            validateBio($bio);
            validateDate($date);


            if($validForm)
            {
                try{
                    $stmt = $con->prepare("INSERT INTO tb_members(member_name, member_age, member_bio, member_date) VALUES(:name, :age, :bio, :date)");
                    $stmt->execute(array(':name'=>$name, ':age'=>$age, ':bio'=>$bio, ':date'=>$date));
                    header('Location:index.php');
                }
                catch(PDOException $ex){
                    echo $ex->getMessage();
                }
            }
            else
            {
                $message = "Something went wrong. Please try again.";
            }
        }
        else{
            //Form has not been seen by the user. Display the form
        }
    ?>

    <!DOCTYPE html>
    <html>
    <head>
    <title>New Member</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
        .row.content {height: 1500px}
        
        /* Set gray background color and 100% height */
        .sidenav {
        background-color: #f1f1f1;
        height: 100%;
        }
        
        /* Set black background color, white text and some padding */
        footer {
        background-color: #555;
        color: white;
        padding: 15px;
        }
        
        /* On small screens, set height to 'auto' for sidenav and grid */
        @media screen and (max-width: 767px) {
        .sidenav {
            height: auto;
            padding: 15px;
        }
        .row.content {height: auto;} 
        }
        .error	{
                color:red;
                font-style:italic;	
            }
    </style>
    <script>
            $(document).ready(function(){
                
                $( "#datepicker" ).datepicker({
                    dateFormat: 'yy-mm-dd'
                });
                
            });
            
    </script>
    </head>
    <body>
         <div class="container-fluid">
            <div class="row content">
               <div class="col-sm-3 sidenav">
               <h4>My Club's Members List</h4>
               <ul class="nav nav-pills nav-stacked">
                  <li class="active"><a href="index.php">Home</a></li>
                  <li><a href="contact.php">Contact</a></li>
                  <?php
                     if ($_SESSION['valid'] == true) {
                  ?>
                        <li><a href="add.php">Add New Member</a></li>
                  <?php
                        }else{
                        }
                  ?>
                  
                  <li><a href="login.php"><span class="glyphicon glyphicon-log-in"></span>Admin Login</a></li>
                  <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span>Log Out</a></li>
               </ul><br>
               </div>

                <div class="col-sm-9"> 
                <h2>Add New Member</h2>
                <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>" method="post">
                    <table class="table table.striped">
                        <tr>
                            <th scope="row">Member Name</th>
                            <td><input type="text" name="txt_name"value="<?php echo $name?>"><span class="error"><?php echo $nameErrMsg ?></span></td>
                        </tr>
                        
                        <tr>
                            <th scope="row">Age</th>
                            <td><input type="number" name="txt_age" value="<?php echo $age ?>"><span class="error"><?php echo $ageErrMsg ?></span></td>
                        </tr>
                        <tr>
                            <th scope="row">Bio</th>
                            <td><textarea rows="5" cols="50" name="txt_bio"><?php echo $bio ?></textarea><span class="error"><?php echo $bioErrMsg ?></span></td>
                        </tr>
                        <tr>
                            <th scope="row">Date</th>
                            <td><input type="text" name="txt_date" id="datepicker" readonly="readonly" value="<?php echo $date ?>"><span class="error"><?php echo $dateErrMsg ?></span></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="submit" class=".btn" name="btn_submit"></td>
                        </tr>
                    </table>
                    <span class="error"><?php echo $message ?></span>
                </form>
                </div>
            </div>
        </div>
        <footer class="container-fluid">
        <p>My Club</p>
        </footer>
    <?php
    }
    ?>