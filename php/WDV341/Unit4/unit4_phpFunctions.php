<!doctype html>
<html>

<head>

    <!--
        WDV 341
        Jake Baxter
        2/10/2018
    -->

    <title>PHP Functions</title>


</head>

<body>

    <form action="#" method="get">
        <p>
            <label for="userDate">Enter a date: </label>
            <input type="text" name="userDate" id="userDate" value="" placeholder="yyyy/mm/dd">
        </p>

        <p>Formatted Date:
            <?php 
                function formatDate() // 1. Accept date input, format to mm/dd/yyyy.
                {
                    $dateInput = $_GET["userDate"];
                    $date = date_create($dateInput);
                    echo date_format($date,"m/d/Y");
                }
        
                formatDate(); // Call function on submit.
            ?>

            <br> Formatted Date (international):

            <?php
         
                function formatDateInt() // 2. Accept date input, format to dd/mm/yyyy.
                {
                    $dateInput = $_GET["userDate"];
                    $date = date_create($dateInput);
                    echo date_format($date,"d/m/Y");
                }
        
                formatDateInt(); // Call function on submit.
            ?>
        </p>

        <p>
            <label for="userString">Enter a comment: </label>
            <input type="text" name="userString" id="userString" placeholder="I go to school at ...">
        </p>

        <p>
            <?php
                function modifyString() // 3. Accept string input.
                {
                    $stringX = $_GET["userString"];
                    
                    $stringX = trim($stringX); // Trim leading or trailing white space.
                    
                    echo "Your comment contains ".strlen($stringX)." characters"."<br>"; // Display the number of characters.
                    
                    echo "Your comment was: ".strtolower($stringX)."<br>";// Display the string as all lowercase.
                    
                    // Display True or False if the string contains "DMACC", case insensitive. 
                    if (stripos($stringX, 'dmacc') == 0 ) {
                        echo "This comment contains DMACC? "."False";
                        }
                    else {
                        echo "This comment contains DMACC? "."True";
                        }
                }
            
              
                modifyString(); // Call function on submit.
            ?>
        </p>

        <p>
            <label for="userNumber1">Enter a number: </label>
            <input type="text" name="userNumber1" id="userNumber1" value="1234567890">
        </p>

        <p>
            <?php 
               
                function formatNumber() // 4. Accept number and display as formatted number. 
                {
                    $numberX = $_GET["userNumber1"];
                    settype($numberX, "float"); //set var type as float. 
                    echo "Your number is: ".number_format($numberX,0,".",","); //run format function and display result.
                }
            
                formatNumber(); // Call function on submit.
            ?>
        </p>
        
        <p>
            <label for="userNumber2">Enter a number: </label>
            <input type="text" name="userNumber2" id="userNumber2" value="123456">
        </p>
        
        <p>
            <?php
                function formatCurrency() // 5. Accept number and display as US currency.
                {
                    $numberY = $_GET["userNumber2"];
                    settype($numberY, "float"); //Set var type as float.
                    echo "Your number as USD currency is: $".number_format($numberY,2,".",","); //run format function and display result.
                }
                
                formatCurrency(); // Call function on submit.
            ?>
        </p>

        <input type="submit" value="Submit">

    </form>



</body>

</html>
